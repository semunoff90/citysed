# coding=utf-8
import pygame
import config

def create_window():
    win = pygame.display.set_mode((config.Resolution['Width'], config.Resolution['Height']))
    pygame.display.set_caption(config.WinName)
    return win

