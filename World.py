# coding=utf-8
import config
import math

class GridClass:
    def __init__(self):
        self.CountCell = config.CountCell #Словарь
        self.CellSize = config.CellSize
        self.CellArr = [[None for i in range(self.CountCell['x'])] for j in range(self.CountCell['y'])]

#======================================================================================================================

class MapClass(GridClass):

    def __init__(self):
        super().__init__()
        self.WorldGrid = GridClass()
        self.ZeroPosition = {'x': 0, 'y': 0}
        self.ENERGY = 0
        self.WATER = 0
        self.MONEY = 10000

#======================================================================================================================

class CameraClass(MapClass):

    def __init__(self):
        super().__init__()
        self.Zoom = config.Camera['Zoom']
        self.SpeedZoom = config.Camera['SpeedZoom']
        self.SpeedCam = config.Camera['PositionSpeed']

    def UP(self):
        if self.ZeroPosition['y'] > -self.CountCell['y']*self.CellSize*self.Zoom:
            self.ZeroPosition['y'] -= self.SpeedCam

    def DOWN(self):
        if self.ZeroPosition['y'] < self.CountCell['y']*self.CellSize*self.Zoom:
            self.ZeroPosition['y'] += self.SpeedCam

    def LEFT(self):
        if self.ZeroPosition['x'] < self.CountCell['x']*self.CellSize*self.Zoom:
            self.ZeroPosition['x'] += self.SpeedCam

    def RIGHT(self):
        if self.ZeroPosition['x'] > -self.CountCell['x']*self.CellSize*self.Zoom:
            self.ZeroPosition['x'] -= self.SpeedCam

    def ZOOM_UP(self):
        if self.Zoom < 10:
            self.Zoom += self.SpeedZoom

    def ZOOM_DOWN(self):
        if self.Zoom > 1:
            self.Zoom -= self.SpeedZoom

    def isSeen(self, pX, pY):
        return 0 + self.ZeroPosition['y'] <= math.fabs(pY)+self.ZeroPosition['y'] <= config.Resolution['Height'] + self.ZeroPosition['y'] and \
               0 + self.ZeroPosition['x'] <= math.fabs(pX)+self.ZeroPosition['x'] <= config.Resolution['Width'] + self.ZeroPosition['x']

    def getCellByPos(self, PosX, PosY):
        X = PosX//config.CellSize//self.Zoom
        Y = PosY//config.CellSize//self.Zoom
        if X < 0 or Y < 0 or X > config.Resolution['Width'] or Y > config.Resolution['Height']:
            return None, None
        else:
            return X, Y

#======================================================================================================================
