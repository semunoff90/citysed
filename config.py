# coding=utf-8
Resolution = {'Width': 800, 'Height': 600}
CellSize = 30
CountCellAuto = {'x': Resolution['Width']//CellSize, 'y': Resolution['Height']//CellSize}
CountCell = {'x': 50, 'y': 50}
Camera = {'Zoom': 1, 'PositionSpeed': 35, 'SpeedZoom': 1}
WinName = "CityeSED"
