# coding=utf-8
import pygame as PG
import config
import Resurse

#============================================GENERAL====================================================================

class ObjectClass:
    def __init__(self):
        self.price = 0
        self.Texture = None
        self.ResurseTexture = None
        self.Size = (1, 1)

    def load_texture(self):
        Texture = PG.image.load(self.ResurseTexture)
        Texture = PG.transform.scale(Texture, (config.CellSize*self.Size[0],
                                               config.CellSize*self.Size[1]))

        self.Texture = Texture

    def rotate(self, value = 90):
        self.Texture = PG.transform.rotate(self.Texture, value)

#===============================================MAIN====================================================================

class BuildClass(ObjectClass):
    def __init__(self):
        super().__init__()
        self.PowerRequired = 0
        self.WaterRequired = 0
        self.RUNBool = True

    def RUN(self, World):
        while self.RUNBool:
            for event in PG.event.get():
                if event.type == PG.QUIT:
                    self.RUNBool = False
            PG.time.wait(10000)
            if World.ENERGY < self.PowerRequired:
                World.MONEY -= self.PowerRequired
            else:
                World.ENERGY -= self.PowerRequired

            if World.WATER < self.WaterRequired:
                World.MONEY -= self.WaterRequired
            else:
                World.WATER -= self.WaterRequired

    def UpData(self, AdjRoad = [0, 0, 0, 0]):
        if AdjRoad[0]:
            self.rotate(90)
        elif AdjRoad[1]:
            self.rotate(180)
        elif AdjRoad[2]:
            self.rotate(270)

#-----------------------------------------------------------------------------------------------------------------------

class RoadClass(ObjectClass):
    def __init__(self):
        super().__init__()
        self.price = 10
        self.ResurseTexture = Resurse.Images['Road1']
        self.load_texture()
        self.AdjRoads = [0, 0, 0, 0]

    def UpData(self):
        swich = self.AdjRoads[0]*8+self.AdjRoads[1]*4+self.AdjRoads[2]*2+self.AdjRoads[3]
        rotateValue = 0
        if swich == 2 or swich == 8 or swich == 10:#1
            self.ResurseTexture = Resurse.Images['Road1']
            rotateValue = 90
        elif swich == 3 or swich == 6 or swich == 9 or swich == 12:#3
            self.ResurseTexture = Resurse.Images['Road3']
            if swich == 3:
                rotateValue = 90
            if swich == 6:
                rotateValue = 180
            if swich == 12:
                rotateValue = 270
        elif swich == 7 or swich == 11 or swich == 13 or swich == 14:#4
            self.ResurseTexture = Resurse.Images['Road4']
            if swich == 11:
                rotateValue = 270
            if swich == 13:
                rotateValue = 180
            if swich == 14:
                rotateValue = 90
        elif swich == 15:#2
            self.ResurseTexture = Resurse.Images['Road2']

        self.load_texture()
        self.rotate(rotateValue)

#-----------------------------------------------------------------------------------------------------------------------

class GrassClass(ObjectClass):
    def __init__(self):
        super().__init__()
        self.ResurseTexture = Resurse.Images['Grass1']
        self.load_texture()

#===========================================SUBCLASS====================================================================

class LeisureClass(BuildClass):
    def __init__(self):
        super().__init__()
        self.Service = 0
        self.BonusRadius = 0
        self.Bonus = 0

    def RUN(self, World):
        while self.RUNBool:
            for event in PG.event.get():
                if event.type == PG.QUIT:
                    self.RUNBool = False
            PG.time.wait(10000)
            if World.MONEY < self.Service:
                pass
            else:
                World.MONEY -= self.Service

            if World.ENERGY < self.PowerRequired:
                World.MONEY -= self.PowerRequired
            else:
                World.ENERGY -= self.PowerRequired

            if World.WATER < self.WaterRequired:
                World.MONEY -= self.WaterRequired
            else:
                World.WATER -= self.WaterRequired

#-----------------------------------------------------------------------------------------------------------------------

class PowerClass(BuildClass):
    def __init__(self):
        super().__init__()
        self.EnergyGenerate = 0
        self.Service = 0
        self.RUNBool = True

    def RUN(self, World):
        while self.RUNBool:
            for event in PG.event.get():
                if event.type == PG.QUIT:
                    self.RUNBool = False
            PG.time.wait(10000)
            if World.MONEY < self.Service:
                pass
            else:
                World.MONEY -= self.Service
                World.ENERGY += self.EnergyGenerate

#-----------------------------------------------------------------------------------------------------------------------

class WaterClass(BuildClass):
    def __init__(self):
        super().__init__()
        self.WaterGenerate = 0
        self.Service = 0
        self.RUNBool = True

    def RUN(self, World):
        while self.RUNBool:
            for event in PG.event.get():
                if event.type == PG.QUIT:
                    self.RUNBool = False
            PG.time.wait(10000)
            if World.MONEY < self.Service:
                pass
            else:
                World.MONEY -= self.Service
                World.WATER += self.WaterGenerate

#============================================OBJECTS====================================================================

class HouseClass(BuildClass):
    def __init__(self):
        super().__init__()
        self.price = 100
        self.PowerRequired = 5
        self.WaterRequired = 5
        self.Tax = 12
        self.ResurseTexture = Resurse.Images['Haus1']
        self.load_texture()

    def RUN(self, World):
        while self.RUNBool:
            for event in PG.event.get():
                if event.type == PG.QUIT:
                    self.RUNBool = False
            PG.time.wait(10000)
            World.MONEY += self.Tax

            if World.ENERGY < self.PowerRequired:
                World.MONEY -= self.PowerRequired
            else:
                World.ENERGY -= self.PowerRequired

            if World.WATER < self.WaterRequired:
                World.MONEY -= self.WaterRequired
            else:
                World.WATER -= self.WaterRequired

#-----------------------------------------------------------------------------------------------------------------------

class ParkClass(LeisureClass):
    def __init__(self):
        super().__init__()
        self.price = 250
        self.WaterRequired = 4
        self.PowerRequired = 4
        self.Service = 5
        self.BonusRadius = 4
        self.Bonus = 3
        self.ResurseTexture = Resurse.Images['Park']
        self.load_texture()

#-----------------------------------------------------------------------------------------------------------------------

class SunPowerClass(PowerClass):
    def __init__(self):
        super().__init__()
        self.ResurseTexture = Resurse.Images['SunPower']
        self.load_texture()
        self.EnergyGenerate = 10
        self.Service = 5

    def UpData(self, AdjRoad = [0, 0, 0, 0]):
        pass

#-----------------------------------------------------------------------------------------------------------------------

class WaterTowerClass(WaterClass):
    def __init__(self):
        super().__init__()
        self.ResurseTexture = Resurse.Images['WaterTower']
        self.load_texture()
        self.WaterGenerate = 10
        self.Service = 5

    def UpData(self, AdjRoad = [0, 0, 0, 0]):
        pass

#=======================================================================================================================
