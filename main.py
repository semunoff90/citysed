# coding=utf-8
import pygame as PG
from pygame.locals import *
import sqlite3
import window
import threading
import config
import World
import Objects as Obj

PG.init()

def DRAW(World, win, idcity):
    run = True
    clock = PG.time.Clock()
    LEFT, RIGHT, UP, DOWN = PG.K_LEFT, PG.K_RIGHT, PG.K_UP, PG.K_DOWN
    B_Z, B_X, B_O = PG.K_z, PG.K_x, PG.K_o
    B_1, B_2, B_3, B_4, B_5, B_6, B_7, B_8, B_9, B_0 = PG.K_1, PG.K_2, \
                                                       PG.K_3, PG.K_4, PG.K_5, PG.K_6, PG.K_7, PG.K_8, PG.K_9, PG.K_0
    B_ESC = PG.K_ESCAPE
    KeyMouse = 0
    ButtonMause = ('None', 'LEFT', 'MIDDLE', 'RIGHT', 'SCROLLUP', 'SCROLLDOWN')
    SelectedBuildText = (" ", "Road", "House", "Power Stantion", "Water Tower", "Park", " ", " ", " ", "Bulldozer")
    BuildSelect = 0
    ClickL = False
    ClickR = False
    KeyPad = False
    f1 = PG.font.Font(None, 36)

    while run:
        clock.tick(30)

        PosMouseX, PosMouseY = PG.mouse.get_pos()

#----------------------------------------EVENTS-------------------------------------------------------------------------

        for event in PG.event.get():
            if event.type == PG.QUIT:
                run = False

            if event.type == PG.MOUSEBUTTONUP:
                if ButtonMause[event.button] == 'LEFT':
                    ClickL = False
                if ButtonMause[event.button] == 'RIGHT':
                    ClickR = False

            if event.type == PG.MOUSEBUTTONDOWN:
                if ButtonMause[event.button] == 'LEFT':
                    ClickL = True
                if ButtonMause[event.button] == 'RIGHT':
                    ClickR = True

            if event.type == PG.KEYUP:
                KeyPad = False

            if event.type == PG.KEYDOWN:
                KeyPad = True

#--------------------------------------SELECTOR-------------------------------------------------------------------------

        if BuildSelect != 0:
            nX, nY = World.getCellByPos(PosMouseX-World.ZeroPosition['x'], PosMouseY-World.ZeroPosition['y'])
            if nX and nY:
                if BuildSelect == 1:
                    if ClickL:
                        BuildRoad(World, nX, nY)
                    if ClickR:
                        DeleteRoad(World, nX, nY)

                if BuildSelect == 2:
                    if ClickL:
                        BuildBuild(World, nX, nY, Obj.HouseClass())
                    if ClickR:
                        DeleteBuild(World, nX, nY, Obj.HouseClass())

                if BuildSelect == 3:
                    if ClickL:
                        BuildBuild(World, nX, nY, Obj.SunPowerClass())
                    if ClickR:
                        DeleteBuild(World, nX, nY, Obj.SunPowerClass())

                if BuildSelect == 4:
                    if ClickL:
                        BuildBuild(World, nX, nY, Obj.WaterTowerClass())
                    if ClickR:
                        DeleteBuild(World, nX, nY, Obj.WaterTowerClass())

                if BuildSelect == 5:
                    if ClickL:
                        BuildBuild(World, nX, nY, Obj.ParkClass())
                    if ClickR:
                        DeleteBuild(World, nX, nY, Obj.ParkClass())


                if BuildSelect == 9:
                    if ClickL:
                        Buldozer(World, nX, nY)

#---------------------------------------BUTTONS-------------------------------------------------------------------------

        keys = PG.key.get_pressed()
        if keys[LEFT] or keys[PG.K_a]:
            World.LEFT()
        if keys[RIGHT] or keys[PG.K_d]:
            World.RIGHT()
        if keys[UP] or keys[PG.K_w]:
            World.DOWN()
        if keys[DOWN] or keys[PG.K_s]:
            World.UP()
        if keys[B_Z]:
            World.ZOOM_UP()
        if keys[B_X]:
            World.ZOOM_DOWN()
        if keys[B_O]:
            World.ZeroPosition['x'] = 0
            World.ZeroPosition['y'] = 0
        if keys[B_ESC]:
            BuildSelect = 0
        if keys[B_1]:
            BuildSelect = 1
        if keys[B_2]:
            BuildSelect = 2
        if keys[B_3]:
            BuildSelect = 3
        if keys[B_4]:
            BuildSelect = 4
        if keys[B_5]:
            BuildSelect = 5
        if keys[B_6]:
            BuildSelect = 6
        if keys[B_7]:
            BuildSelect = 7
        if keys[B_8]:
            BuildSelect = 8
        if keys[B_9]:
            BuildSelect = 9
        if keys[B_0]:
            BuildSelect = 0
        if keys[PG.K_q]:
            SAVE(World, idcity)

#------------------------------------------TEXT-------------------------------------------------------------------------

        textmoney = f1.render(str(World.MONEY)+'$', 1, (0, 0, 0))
        textenergy = f1.render(str(World.ENERGY)+'MWt', 1, (0, 0, 0))
        textwater = f1.render(str(World.WATER)+'L', 1, (0, 0, 0))
        textselectbuild = f1.render(SelectedBuildText[BuildSelect], 1, (0, 0, 0))

#----------------------------DRAW---------------------------------------------------------------------------------------

        win.fill((124, 184, 220))
        SizeZoom = World.Zoom*World.CellSize
        for i in range( World.CountCell['x']):
            win.blit(textmoney, (10, 50))
            win.blit(textenergy, (10, 80))
            win.blit(textwater, (10, 110))
            win.blit(textselectbuild, (10, 550))
            if World.isSeen(i*SizeZoom+World.ZeroPosition['x'], 0):
                Draw_Obj(World, i, win)
        PG.display.update()

    PG.quit()

#=======================================================================================================================

def clear_screen(run, clock):
    while run:
        clock.tick(5)
        win.fill((0, 0, 0))
        PG.display.update()

#-----------------------------------------------------------------------------------------------------------------------

def Draw_Obj(World, i, win):
    SizeZoom = World.Zoom*World.CellSize
    for j in range(World.CountCell['y']):
        if World.CellArr[i][j] is not None and \
                World.isSeen(i*SizeZoom+World.ZeroPosition['x'], j*SizeZoom+World.ZeroPosition['y']):

            World.CellArr[i][j].Texture = PG.transform.scale(World.CellArr[i][j].Texture,
                                                    (World.CellArr[i][j].Size[0]*SizeZoom,
                                                    World.CellArr[i][j].Size[1]*SizeZoom))
            win.blit(World.CellArr[i][j].Texture, (i*SizeZoom+World.ZeroPosition['x'],
                                                   j*SizeZoom+World.ZeroPosition['y']))

#-----------------------------------------------------------------------------------------------------------------------

def GetAdj(World, nX, nY):
    Objects = []
    Objects.append(World.CellArr[nX-1][nY])
    Objects.append(World.CellArr[nX-1][nY-1])
    Objects.append(World.CellArr[nX][nY-1])
    Objects.append(World.CellArr[nX+1][nY-1])
    Objects.append(World.CellArr[nX+1][nY])
    Objects.append(World.CellArr[nX+1][nY+1])
    Objects.append(World.CellArr[nX][nY+1])
    Objects.append(World.CellArr[nX-1][nY+1])
    return Objects

#-----------------------------------------------------------------------------------------------------------------------

def AdjIsRoad(World, nX, nY):
    AdjRoad = [0, 0, 0, 0]
    if type(World.CellArr[nX-1][nY]) is type(Obj.RoadClass()):
        AdjRoad[0]=1
    if type(World.CellArr[nX][nY+1]) is type(Obj.RoadClass()):
        AdjRoad[1]=1
    if type(World.CellArr[nX+1][nY]) is type(Obj.RoadClass()):
        AdjRoad[2]=1
    if type(World.CellArr[nX][nY-1]) is type(Obj.RoadClass()):
        AdjRoad[3]=1

    if AdjRoad.count(1):
        return AdjRoad
    else:
        return None

#-----------------------------------------------------------------------------------------------------------------------

def GetAdjByRadius(World, nX, nY, R):
    L, H = nX-R, nY-R
    Objects = []
    for i in range(R*2):
        for j in range(R*2):
            Objects.append(World.CellArr[L+i][H+j])
    return Objects

#=======================================================================================================================
#---------------------------------------------ROAD----------------------------------------------------------------------

def BuildRoad(World, nX, nY):
    if World.MONEY < World.CellArr[nX][nY].price:
        return
    if not type(World.CellArr[nX][nY]) is type(Obj.GrassClass()):
        return

    AdjNewObj = [0, 0, 0, 0]
    Objects = []

    if type(World.CellArr[nX-1][nY]) is type(Obj.RoadClass()):
        World.CellArr[nX-1][nY].AdjRoads[2]=1
        AdjNewObj[0] = 1
        Objects.append(World.CellArr[nX-1][nY])

    if type(World.CellArr[nX][nY+1]) is type(Obj.RoadClass()):
        World.CellArr[nX][nY+1].AdjRoads[1]=1
        AdjNewObj[3] = 1
        Objects.append(World.CellArr[nX][nY+1])

    if type(World.CellArr[nX+1][nY]) is type(Obj.RoadClass()):
        World.CellArr[nX+1][nY].AdjRoads[0]=1
        AdjNewObj[2] = 1
        Objects.append(World.CellArr[nX+1][nY])

    if type(World.CellArr[nX][nY-1]) is type(Obj.RoadClass()):
        World.CellArr[nX][nY-1].AdjRoads[3]=1
        AdjNewObj[1] = 1
        Objects.append(World.CellArr[nX][nY-1])

    World.CellArr[nX][nY] = Obj.RoadClass()
    World.CellArr[nX][nY].AdjRoads = AdjNewObj
    World.CellArr[nX][nY].UpData()
    World.MONEY -= World.CellArr[nX][nY].price

    if len(Objects) > 0:
        for O in Objects:
            O.UpData()

#-----------------------------------------------------------------------------------------------------------------------

def DeleteRoad(World, nX, nY):
    if not type(World.CellArr[nX][nY]) is type(Obj.RoadClass()):
        return


    Objects = []
    Objects.append(World.CellArr[nX-1][nY])
    Objects.append(World.CellArr[nX][nY+1])
    Objects.append(World.CellArr[nX+1][nY])
    Objects.append(World.CellArr[nX][nY-1])

    for O in Objects:
        if not type(O) is type(Obj.GrassClass()) and not type(O) is type(Obj.RoadClass()):
            return

    if type(World.CellArr[nX-1][nY]) is type(Obj.RoadClass()):
        World.CellArr[nX-1][nY].AdjRoads[2]=0

    if type(World.CellArr[nX][nY+1]) is type(Obj.RoadClass()):
        World.CellArr[nX][nY+1].AdjRoads[1]=0

    if type(World.CellArr[nX+1][nY]) is type(Obj.RoadClass()):
        World.CellArr[nX+1][nY].AdjRoads[0]=0

    if type(World.CellArr[nX][nY-1]) is type(Obj.RoadClass()):
        World.CellArr[nX][nY-1].AdjRoads[3]=0

    World.CellArr[nX][nY] = Obj.GrassClass()
    World.CellArr[nX][nY].load_texture()

    if len(Objects) > 0:
        for O in Objects:
            if type(O) is type(Obj.RoadClass()):
                O.UpData()

#---------------------------------------------------------BUILDS--------------------------------------------------------

def BuildBuild(World, nX, nY, newObject):
    if World.MONEY < World.CellArr[nX][nY].price:
        return
    if not type(World.CellArr[nX][nY]) is type(Obj.GrassClass()):
        return
    AdjRoad = AdjIsRoad(World, nX, nY)

    if AdjRoad and not type(World.CellArr[nX][nY]) is type(Obj.RoadClass()):
        World.CellArr[nX][nY] = newObject
        World.CellArr[nX][nY].UpData(AdjRoad)
    else: return


    World.MONEY -= World.CellArr[nX][nY].price
    threading.Thread(target=World.CellArr[nX][nY].RUN, args=(World,)).start()
    if isinstance(World.CellArr[nX][nY], Obj.LeisureClass):
        Ob = GetAdjByRadius(World, nX, nY, World.CellArr[nX][nY].BonusRadius)
        for O in Ob:
            if type(O) is type(Obj.HouseClass()):
                O.Tax += World.CellArr[nX][nY].Bonus
            elif isinstance(O, (Obj.PowerClass, Obj.WaterClass)):
                if O.Service - World.CellArr[nX][nY].Bonus < 0:
                    O.Service = 1
                else:
                    O.Service -= World.CellArr[nX][nY].Bonus

#-----------------------------------------------------------------------------------------------------------------------

def DeleteBuild(World, nX, nY, Object):
    if type(World.CellArr[nX][nY]) is type(Object):
        World.CellArr[nX][nY].RUNBool = False

        if isinstance(World.CellArr[nX][nY], Obj.LeisureClass):
            print("LEISURE_DELL")
            Ob = GetAdjByRadius(World, nX, nY, World.CellArr[nX][nY].BonusRadius)
            for O in Ob:
                if type(O) is type(Obj.HouseClass()):
                    O.Tax -= World.CellArr[nX][nY].Bonus
                elif isinstance(O, (Obj.PowerClass, Obj.WaterClass)):
                    O.Service += World.CellArr[nX][nY].Bonus

        World.CellArr[nX][nY] = Obj.GrassClass()

#-------------------------------------------BULLDOZER-------------------------------------------------------------------

def Buldozer(World, nX, nY):
    if type(World.CellArr[nX][nY]) is type(Obj.RoadClass()):
        DeleteRoad(World, nX, nY)
    else:
        DeleteBuild(World, nX, nY, World.CellArr[nX][nY])

#=======================================================================================================================

def SAVE(World, idcity):
    print("saving///")
    conn = sqlite3.connect("save.db") # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()

    sql = "DELETE FROM city WHERE idcity = '"+str(idcity)+"'"

    cursor.execute(sql)
    conn.commit()

    sql = "DELETE FROM indicators WHERE idcity = '"+str(idcity)+"'"
    cursor.execute(sql)
    conn.commit()

    # Вставляем множество данных в таблицу используя безопасный метод "?"
    Objects = []
    for i in range(config.CountCell['y']):
        for j in range(config.CountCell['x']):
            if not type(World.CellArr[j][i]) is type(Obj.GrassClass()):
                Objects.append((None, j, i, str(type(World.CellArr[j][i])), idcity))

    print(Objects)
    cursor.executemany("INSERT INTO city VALUES (?,?,?,?,?)", Objects)

    indicators = (None, World.MONEY, World.ENERGY, World.WATER, idcity)
    cursor.execute("""INSERT INTO indicators VALUES (?,?,?,?,?)""", indicators)
    conn.commit()
    print("DONE")


def LOAD(World, idcity):
    print("loading///")
    conn = sqlite3.connect("save.db") # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()

    sql = "SELECT * FROM city WHERE idcity = '"+str(idcity)+"'"

    cursor.execute(sql)
    conn.commit()
    Objects = cursor.fetchall()

    sql = "SELECT * FROM indicators WHERE idcity = '"+str(idcity)+"'"
    cursor.execute(sql)
    conn.commit()
    Indicators = cursor.fetchone()

    for O in Objects:
        if O[3] == str(type(Obj.RoadClass())):
            BuildRoad(World, O[1], O[2])
    for O in Objects:
        if O[3] == str(type(Obj.SunPowerClass())):
            BuildBuild(World, O[1], O[2], Obj.SunPowerClass())
        elif O[3] == str(type(Obj.WaterTowerClass())):
            BuildBuild(World, O[1], O[2], Obj.WaterTowerClass())
        elif O[3] == str(type(Obj.HouseClass())):
            BuildBuild(World, O[1], O[2], Obj.HouseClass())
        elif O[3] == str(type(Obj.ParkClass())):
            BuildBuild(World, O[1], O[2], Obj.ParkClass())

    World.MONEY, World.ENERGY, World.WATER = Indicators[1], Indicators[2], Indicators[3]
    print("DONE")

#=======================================================================================================================

def MENU_DRAW(win, World):
    run = True
    ButtonMause = ('None', 'LEFT', 'MIDDLE', 'RIGHT', 'SCROLLUP', 'SCROLLDOWN')
    clock = PG.time.Clock()
    CX, CY = config.Resolution['Width']//2, config.Resolution['Height']//2
    f1 = PG.font.Font(None, 36)
    f2 = PG.font.Font(None, 42)
    textNewGame = f1.render("New Game", 1, (0, 0, 0))
    textLoad = f1.render("Load Game", 1, (0, 0, 0))
    textEXIT = f1.render("EXIT", 1, (0, 0, 0))
    textSelectNewGame = f2.render("New Game", 1, (242, 103, 93))
    textSelectLoad = f2.render("Load Game", 1, (242, 103, 93))
    textSelectEXIT = f2.render("EXIT", 1, (242, 103, 93))
    M_MENU = {'NG': textNewGame, 'LG':textLoad, 'E': textEXIT}
    MS_MENU = {'NG': textSelectNewGame, 'LG':textSelectLoad, 'E': textSelectEXIT}
    state = 0
    ClickL = False
    while run:
        clock.tick(30)

        PosMouseX, PosMouseY = PG.mouse.get_pos()

#----------------------------------------EVENTS-------------------------------------------------------------------------

        for event in PG.event.get():
            if event.type == PG.QUIT:
                run = False

            if event.type == PG.MOUSEBUTTONUP:
                if ButtonMause[event.button] == 'LEFT':
                    ClickL = False
                if ButtonMause[event.button] == 'RIGHT':
                    ClickR = False

            if event.type == PG.MOUSEBUTTONDOWN:
                if ButtonMause[event.button] == 'LEFT':
                    ClickL = True
                if ButtonMause[event.button] == 'RIGHT':
                    ClickR = True

#----------------------------------------TEXT---------------------------------------------------------------------------
        win.fill((124, 184, 220))
        keys = PG.key.get_pressed()
        if keys[PG.K_ESCAPE]:
            state = 0

        if state == 0:
            state = MAIN_MENU_DRAW(win, PosMouseX, PosMouseY, CX, CY, M_MENU, MS_MENU, state, ClickL)
        elif state == 1:
            NewGame(win, World)
        elif state == 2:
            LOAD_MENU_DRAW(win, PosMouseX, PosMouseY, CX, CY, state, ClickL, World)

        PG.display.update()

#----------------------------MAIN-DRAW----------------------------------------------------------------------------------

def MAIN_MENU_DRAW(win, PosMouseX, PosMouseY, CX, CY, M_MENU, MS_MENU, state, ClickL):
#----------------------------DRAW---------------------------------------------------------------------------------------

    if CX < PosMouseX < CX + 150 and CY < PosMouseY < CY + 25:
        win.blit(MS_MENU['NG'], (CX, CY))
        if ClickL:
            state = 1
    else:
        win.blit(M_MENU['NG'], (CX, CY))

    if CX < PosMouseX < CX + 150 and CY + 30 < PosMouseY < CY + 30 + 25:
        win.blit(MS_MENU['LG'], (CX, CY+30))
        if ClickL:
            state = 2
    else:
        win.blit(M_MENU['LG'], (CX, CY+30))

    if CX < PosMouseX < CX + 150 and CY + 60 < PosMouseY < CY + 60 + 25:
        win.blit(MS_MENU['E'], (CX, CY+60))
        if ClickL:
            state = 3
    else:
        win.blit(M_MENU['E'], (CX, CY+60))

    return state

#-----------------------------------------------------------------------------------------------------------------------

def LOAD_MENU_DRAW(win, PosMouseX, PosMouseY, CX, CY, state, ClickL, World):
    f1 = PG.font.Font(None, 36)
    f2 = PG.font.Font(None, 42)
    conn = sqlite3.connect("save.db") # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()
    cursor.execute("""SELECT * FROM cityes""")
    conn.commit()
    cityes = cursor.fetchall()
    i = 0
    for C in cityes:
        if CX-150 < PosMouseX < CX and CY+i < PosMouseY < CY + i+30:
            win.blit(f2.render("City - "+str(C[0]), 1, (242, 103, 93)), (CX-150, CY+i))
            if ClickL:
                LOAD(World, C[0])
                DRAW(World, win, C[0])
        else:
            win.blit(f1.render("City - "+str(C[0]), 1, (0, 0, 0)), (CX-150, CY+i))

        i+=30
    return state



#-----------------------------------------------------------------------------------------------------------------------

def NewGame(win, World):
    conn = sqlite3.connect("save.db") # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()
    cursor.execute("""INSERT INTO cityes VALUES (?, ?)""", (None, 1))
    cursor.execute("""SELECT MAX(id) FROM cityes""")
    conn.commit()
    idcity = cursor.fetchone()
    DRAW(World, win, idcity[0])

#-----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    win = window.create_window()
    MyCity = World.CameraClass()
    MyCity.CellArr = [[Obj.GrassClass() for i in range(MyCity.CountCell['x'])] for i in range(MyCity.CountCell['y'])]
    MENU_DRAW(win, MyCity)
    #LOAD(MyCity, 1)
    #DRAW(MyCity, win)
